# MPC Pad

![MPC World Pad](doc/front.jpg)

A small macropad to control mpc client of mpd.

Keyboard Maintainer: [coliss86](https://gitlab.com/coliss86)

## Electronic

The pinout of the atmega can [be found here](https://i.pinimg.com/originals/a5/04/57/a504570b03287da7cdd419270ec49603.jpg)

Inside the mess :

![MPC](doc/inside.jpg)

In situation :

![MPC](doc/garage.jpg)
